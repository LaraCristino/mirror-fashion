
      <?php
        $cabecalho_css = '<link rel="stylesheet" href="/css/style.css">';
        $cabecalho_css = $cabecalho_css . '<link rel="stylesheet" href="/css/reset.css">';
        $cabecalho_title = "Produto da Mirror Fashion";
        include ("cabecalho.php");
      ?>

      <div class="produto-back">
        <div class="container">
          <div class="produto">
            <h1>Camiseta Echo</h1>
            <span>por apenas R$ 49,90</span>

            <form action="checkout.php" method="post">
              <fieldset class="cores">
                <legend>Escolha a cor:</legend>

                <input type="hidden" name="nome" value="Camiseta Echo">
                <input type="hidden" name="preco" value="49.90">

                <input type="radio" name="cor" value="verde" id="verde" checked>
                  <label for="verde">
                    <img src="img/produtos/foto2-verde.png" alt="verde">
                  </label>

                <input type="radio" name="cor" value="rosa" id="rosa">
                  <label for="rosa">
                    <img src="img/produtos/foto2-rosa.png" alt="rosa">
                  </label>

                <input type="radio" name="cor" value="azul" id="azul">
                  <label for="azul">
                    <img src="img/produtos/foto2-azul.png" alt="azul">
                  </label>


              </fieldset>

              <fieldset class="tamanhos">
                <legend>Escolha o tamanho:</legend>
                <input type="range" min="36" max="46" name="tamanho" value="42"
                    id="tamanho" step="2">
                <output for="tamanho" name="valortamanho">42</output>
                <script>
                  var inputTamanho = document.querySelector('[name=tamanho]')
                  var outputTamanho = document.querySelector('[name=valortamanho]')

                  inputTamanho.oninput = mostraTamanho

                  function mostraTamanho() {
                    outputTamanho.value = inputTamanho.value
                  }
                </script>
              </fieldset>

              <input type="submit" class="comprar" value="Comprar">

            </form>
          </div>

          <section class="detalhes">
            <h2>Detalhes do Produto</h2>
            <p>Essa é a melhor camiseta que você já viu.
              Excelente material italiano com estampa desenhada pelos artesãos da
              comunidade de Krotor nas ilhas gregas. Compre já e receba hoje mesmo
              pela nossa entrega ajato.</p>

            <table>
              <thead>
                <tr>
                  <th>Característica</th>
                  <th>Detalhe</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Modelo</td>
                  <td>Camiseta Echo</td>
                </tr>
                <tr>
                  <td>Cores</td>
                  <td>Azul, Rosa e Verde</td>
                </tr>
                <tr>
                  <td>Lavagem</td>
                  <td>Lavar a mão</td>
                </tr>
              </tbody>
            </table>
          </section>

        </div>
      </div>

    </body>
    <?php include ("rodape.php");
    ?>
</html>
