<!doctype html>
  <head>
    <title>Checkout Mirror Fashion></title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> <!-- diz que o site é responsivo -->
    <!--<link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- tema bootstrapn que deixa o navbar e algumas funções azuis. Se quiser usar, troca o link do bootstrap normal-->
        <link rel="stylesheet" href="css/bootstrap-flatly.css">

    <!--<link href="css/bootstrap.css" rel="stylesheet">-->

    <!-- [if lt IE 9] -->
    <script type="javascript" src="html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <!-- [endif] -->

    <style>
    .navbar {
      margin: 0;
    }
    .navbar .glyphicon {
      color: yellow;
    }
    .form-control:invalid {
      border: 1px solid #cc0000;
    }
    </style>

  </head>

  <body>

    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="sobre.php">Sobre</a></li>
            <li><a href="#">Ajuda</a></li>
            <li><a href="#">Perguntas Frequentes</a></li>
            <li><a href="#">Entre em Contato</a></li>

            <span class="glyphicon glyphicon-home"></span>
            <span class="glyphicon glyphicon-question-sign"></span>
            <span class="glyphicon glyphicon-list-all"></span>
            <span class="glyphicon glyphicon-bullhorn"></span>
          </ul>

        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <div class="jumbotron">
      <div class="container">
        <!-- h1 e p que já tinhamos -->
        <h1>Ótima escolha!</h1>
        <p>Obrigado por comprar na Mirror Fashion!
          Preencha seus dados para efetivar a compra. </p>
      </div><!--fim .container dentro do jumbotron -->
    </div><!-- fim .jumbotron -->

    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h2 class="panel-title">Sua compra</h2>
            </div><!--...aqui vai <dl> que já temos hoje-->
            <div class="panel-body">
              <img  src="img/produtos/foto<?= $_POST["id"] ?>-<?= $_POST["cor"] ?>.png" alt="Camiseta Echo"
                class="img-thumbnail img-responsive">
              <!--...aqui vai <dl> que já temos hoje...-->
              <dl>
                <dt>Produto</dt>
                <dd><?= $_POST['nome'] ?></dd>
                <dt>Preço</dt>
                <dd id="preco"><?= $_POST['preco'] ?></dd>
                <dt>Cor</dt>
                <dd><?= $_POST['cor'] ?></dd>
                <dt>Tamanho</dt>
                <dd><?= $_POST['tamanho'] ?></dd>
              </dl>

              <div class="form-group">
                <label for="qt">Quantidade</label>
                <input id="qt" class="form-control" type="number" min="0"
                    max="99" value="1">
              </div>
              <div class="form-group">
                <label for="total">Total</label>
                <output for="qt preco" id="total" class="form-control">
                  R$<? $_POST["preco"] ?>
                </output>
              </div>


            </div>
          </div><!--fim .panel-body-->
        </div>

        <div class="col-sm-8">
            <form>
              <div class="row">
                <fieldset class="col-md-6">
                  <legend>Dados pessoais</legend>

                  <div class="form-group">
                    <label for="nome">Nome Completo</label>
                    <input type="text" class="form-control" name="nome"
                      id="nome" autofocus required>
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <div class="input-group">
                      <span class="input-group-addon">@</span>
                      <input type="email" class="form-control" name="email"
                        id="email" placeholder="email@exemplo.com" required="required">
                      <!--<script>
                        var inputEmail = document.querySelector('input[type="email"]')

                        inputEmail.oninvalid = mensagemErro

                        function mensagemErro() {
                          //remove mensagens de erros antigas
                          this.setCustomValidity("");
                          //reexecuta validação
                          if (!this.validity.valid) {
                            //se invalido, coloca mensagem de erro
                            this.setCUstomValidity("Email inválido");
                          }
                        }
                      </script>-->
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="cpf">CPF</label>
                    <input type="text" class="form-control" name="cpf"
                        id="cpf" placeholder="000.000.000-00" required>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="sim" name="spam" checked>
                      Quero receber spam da Mirror Fashion
                    </label>
                  </div>

                </fieldset>


                <fieldset class="col-md-6">
                  <legend>Cartão de Crédito</legend>

                  <div class="form-group">
                    <label for="numero-cartao">Número - CVV</label>
                    <input type="text" class="form-control"
                        id="numero-cartao" name="numero-cartao" required>
                  </div>

                  <div class="form-group">
                    <label for="bandeira-cartao">Bandeira</label>
                    <select class="form-control" name="bandeira-cartao"
                        id="bandeira-cartao" required
                      <option value="master">Mastercard</option>
                      <option value="visa">Visa</option>
                      <option value="amex">American Express</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="validade-cartao">Validade</label>
                    <input type="month" class="form-control"
                        id="validade-cartao" name="validade-cartao" required>
                  </div>

                  <button type="submit" class="btn btn-primary btn-lg pull right" name="enviar">
                    <span class="glyphicon glyphicon-thumbs-up"></span>
                    Confirmar Pedido
                  </button>

                </fieldset>
            </form>
          </div>
        </div><!--fim .painel-->
      </div><!-- fim da primeira .row -->
    </div><!-- fim do .container da página -->


    <script src="js/total.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>

  </body>
</html>
