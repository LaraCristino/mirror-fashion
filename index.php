  <?php
    $cabecalho_css = '<link rel="stylesheet" href="/css/style.css">';
    $cabecalho_css = $cabecalho_css . '<link rel="stylesheet" href="/css/reset.css">';
    $cabecalho_title = "Mirror Fashion";
    include ("cabecalho.php");
  ?>

  <main class="container destaque">

    <section class="busca">
      <h2>Busca</h2>
      <form>
        <input type="search" placeholder="Buscar">
        <input type="image" src="img/busca.png" class="lupa" >
      </form>
    </section>

    <section class="menu-departamentos">
      <h2>Departamentos</h2>
      <nav>
        <ul>
          <li><a href="#">Blusas e Camisas</a></li>
          <li><a href="#">Calças</a></li>
          <li><a href="#">Saias</a></li>
          <li><a href="#">Vestidos</a></li>
          <li><a href="#">Sapatos</a></li>
          <li><a href="#">Bolsas e carteiras</a></li>
          <li><a href="#">Acessórios</a></li>
        </ul>
      </nav>
  </section>

    <img src="img/destaque-home.png" alt="Promoção Big City Nights">
    <script>
      var banners = ["img/destaque-home.png", "img/destaque-home-2.png"];
      var bannerAtual = 0;

      function trocaBanner() {
        bannerAtual = (bannerAtual + 1) % 2;
        document.querySelector('.destaque img'.src = banners = [bannerAtual]);
      }

      setInterval(trocaBanner, 4000);
    </script>

  <div class="container paineis">
    <section class="painel novidades">
      <h2>Novidades</h2>
      <ol>
        <li><a href="produto.php">
          <figure>
            <img src="img/produtos/miniatura1.png" alt="camiseta">
            <figcaption>Terninho Florido</figcaption>
          </figure>
        </a></li>
        <li>
          <a href="produto.php">
            <figure>
              <img src="img/produtos/miniatura2.png" alt="camiseta">
              <figcaption>Bad to the bone</figcaption>
            </figure>
        </a>
      </li>
        <li>
          <a href="produto.php">
            <figure>
              <img src="img/produtos/miniatura3.png" alt="camiseta">
              <figcaption>Cardigã Echo</figcaption>
            </figure>
        </a>
      </li>
        <li>
          <a href="produto.php">
            <figure>
              <img src="img/produtos/miniatura4.png" alt="camiseta">
              <figcaption>Dark Jacket</figcaption>
            </figure>
        </a>
      </li>
        <li>
          <a href="produto.php">
            <figure>
              <img src="img/produtos/miniatura5.png" alt="camiseta">
              <figcaption>Blue Basic</figcaption>
            </figure>
        </a>
      </li>
        <li>
          <a href="produto.php">
            <figure>
              <img src="img/produtos/miniatura6.png" alt="camiseta">
              <figcaption>Pink Fit</figcaption>
            </figure>
        </a>
      </li>

      </ol>
      </section>

      <section class="painel mais-vendidos">
        <h2>Mais vendidos</h2>
        <ol>
          <li><a href="produto.php">

          <figure>
            <img src="img/produtos/miniatura15.png" alt="camiseta">
            <figcaption>Romantic Blue</figcaption>
          </figure>
        </a></li>
        <li>
          <a href="produto.php">
            <figure>
              <img src="img/produtos/miniatura8.png" alt="camiseta">
              <figcaption>Basic One</figcaption>
            </figure>
        </a>
      </li>
        <li>
          <a href="produto.php">
            <figure>
              <img src="img/produtos/miniatura9.png" alt="camiseta">
              <figcaption>Chess</figcaption>
            </figure>
        </a>
      </li>
        <li>
          <a href="produto.php">
            <figure>
              <img src="img/produtos/miniatura10.png" alt="camiseta">
              <figcaption>Manga Longa</figcaption>
            </figure>
        </a>
      </li>
        <li>
          <a href="produto.php">
            <figure>
              <img src="img/produtos/miniatura11.png" alt="camiseta">
              <figcaption>Short Folha</figcaption>
            </figure>
        </a>
      </li>
        <li>
          <a href="produto.php">
            <figure>
              <img src="img/produtos/miniatura12.png" alt="camiseta">
              <figcaption>Camisa Azul</figcaption>
            </figure>
        </a>
      </li>

      </ol>
      </section>

    </div>


    </main>

    <?php
      include ("rodape.php");
    ?>

  </body>
</html>
